﻿using ConsoleApplication2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            //Person person = new Person();
            //person.Name = "qwerty";
            //Console.WriteLine(person.Name);
            //person.Manager();
            //Console.ReadLine();
            Console.WriteLine("Hi!\nEnter the name of the Employee");
            Person person = new Person();
            person.Name = Console.ReadLine();
            Console.WriteLine("Choose a profession for " + person.Name + ":\n1) Security\n2) Manager\n3) Janitor\n4) Seller \n");

            var ProfessionNumber = Convert.ToInt32(Console.ReadLine());

            switch (ProfessionNumber)
            {
                case 1:

                    break;

                case 2:

                    break;

                case 3:

                    break;

                case 4:

                    break;
            }
        }
    }

    public interface IEmployee
    {
        double Salary { get; set; }
        string Name { get; set; }
        void DoWork();
    }

    public class Person : IEmployee
    {
        public string Name { get; set; }
        public double Salary { get; set; }

        public void DoWork()
        {
        }
    }


    public class Security : Person
    {

    }

    public class Manager
    {

    }

    public class Janitor
    {

    }

    public class Seller
    {

    }


    public class Bank
    {
        private List<Person> _employee;

        public Bank(double bankMoney)
        {
            this.Money = bankMoney;
            this._employee = new List<Person>();
        }

        public double Money { get; set; }

        public void ListOfWorkers() //получение списка сотрудников
        {
        }

        public void HireWorker()
        {
        }

        public void FireWorker()
        {
        }

        public double PaySalary()
        {
            var money = this.Money;

            // TODO: add passing thru list of employes and pay them from bank money

            // TODO: We should return amount of paid money:
            //return money - this.Money;

            // temp
            return money - 100;
        }

        public void WorkDayStart()
        {

        }
    }

}


/*
Нужно будет создать банковскую систему
) Создаешь интерфейс (IEmployee) в нем -
св-ва:
- Salary
- Name
метод:
- DoWork()
2) Создаешь несколько классов (смотри выше) унаследованных/реализующих от интерфейса (IEmployee).
DoWork() для каждого из классов будет делать запись в консоль, в зависимости от своего рода занятий (Кассир - "Count the money." и т.д.)
будут профессии унаследованные от интерфейса работника
(как реализуешь шаг 2 не важно, есть 2 способа хороших по реализации)
Профессии должны быть Охранник, Манагер, Уборщик, Кассир
у каждой профессии своя зп
3) Будет банк
он может нанимать, увольнять, платить зп, стартовать рабочий день, предоставлять список работников
у него будет свой капитал
пока все, далее еще добавится холдинг
для начала реализуй 1 2 3
интерфейс пока не пиши, чисто проверь чтобы все работало
далее интерфейс будет пилить, чтобы работать с системой через него
Вывод инфы тоже будет с интерфейсом
все понятно?
 */
